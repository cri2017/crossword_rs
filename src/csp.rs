use std::collections::HashMap;
use std::collections::HashSet;

#[derive(Clone)]
#[derive(PartialEq, Eq, Hash)]
#[derive(Debug)]
pub enum Direction {
    Vertical,
    Horizontal
}

#[derive(Clone)]
#[derive(PartialEq, Eq, Hash)]
#[derive(Debug)]
pub struct Word {
    pub id: String,
    pub direction: Direction,
    pub nodes: Vec<(usize, usize)>
}

#[derive(Clone)]
#[derive(PartialEq, Eq, Hash)]
#[derive(Debug)]
pub struct Constraint {
    pub hw: Word, // horizontal word
    pub vw: Word,
    pub hi: usize, // horizontal index
    pub vi: usize
}

// minimum remaining values
fn mrv(un: &HashMap<Word, HashSet<Vec<char>>>) -> Word {
    un.iter().filter(|&(_, v)| { v.len() > 0 }).
        min_by_key(|&(_, v)| { v.len() }).
        unwrap().0.clone()
}

fn test_constraints(var: Word,
                    val: Vec<char>,
                    assig: &HashMap<Word, Vec<char>>,
                    rs_w: &HashMap<Word, Vec<Constraint>>)
    -> bool
{
    for constraint in &rs_w[&var] {
        if var.direction == Direction::Vertical {
            if !assig.contains_key(&constraint.hw) {
                continue;
            }
            let char1 = val[constraint.vi];
            let char2 = assig[&constraint.hw][constraint.hi];
            if char1 != char2 {
                return false;
            }
        } else {
            if !assig.contains_key(&constraint.vw) {
                continue;
            }
            let char1 = val[constraint.hi];
            let char2 = assig[&constraint.vw][constraint.vi];
            if char1 != char2 {
                return false;
            }
        }
    }
    true
}

fn update_domains(var: &Word,
                  val: &Vec<char>,
                  un: &HashMap<Word, HashSet<Vec<char>>>,
                  rs_w: &HashMap<Word, Vec<Constraint>>)
    -> Option<HashMap<Word, HashSet<Vec<char>>>>
{
    let mut un = un.clone();
    for constraint in &rs_w[&var] {
        let (c, var2, n2) = if var.direction == Direction::Vertical {
            (val[constraint.vi], constraint.hw.clone(), constraint.hi)
        } else {
            (val[constraint.hi], constraint.vw.clone(), constraint.vi)
        };
        if !un.contains_key(&var2) {
            continue;
        }
        let new_domain: HashSet<Vec<char>> = un[&var2].iter().cloned().filter(|val2| {
            val2[n2] == c
        }).collect();
        if new_domain.len() == 0 {
            return None
        }
        un.insert(var2, new_domain);
    }
    Some(un)
}

pub fn solve(assig: &HashMap<Word, Vec<char>>,
             un: &HashMap<Word, HashSet<Vec<char>>>,
             rs_w: &HashMap<Word, Vec<Constraint>>,
             rs: &Vec<Constraint>)
    -> Option<HashMap<Word, Vec<char>>>
{
    if un.len() == 0 {
        return Some(assig.clone());
    }
    let var = mrv(un);

    for val in &un[&var.clone()] {
        if test_constraints(var.clone(), val.clone(), assig, rs_w) {
            let mut un = match update_domains(&var, &val, &un, &rs_w) {
                Some(un) => un,
                None => continue,
            };
            let mut assig = assig.clone();
            assig.insert(var.clone(), val.clone());
            un.remove(&var.clone());

            match solve(&assig, &un, rs_w, rs) {
                Some(s) => return Some(s),
                None => ()
            };
        }
    }

    None
}
