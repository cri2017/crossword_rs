//use std::env;
use std::fs::File;
//use std::io::prelude::*;
use std::io::BufReader;
use std::io::BufRead;
use std::collections::HashMap;
use std::collections::HashSet;
use std::process::exit;

mod csp;
use csp::*;

fn del_redundants(words: Vec<Word>) -> Vec<Word> {
    words.iter().cloned().filter(|w| {
        for w2 in words.iter() {
            if w.id == w2.id {
                continue;
            }
            if w2.nodes.contains(&w.nodes[0]) {
                return false;
            }
        }
        true
    }).collect()
}

fn print_solution(cross: Vec<Vec<String>>,
                  sol: HashMap<Word, Vec<char>>)
{
    let mut cross = cross.clone();
    for (word, val) in sol {
        for (node, c) in word.nodes.iter().zip(val) {
            let (x, y) = *node;
            cross[y][x] = vec![c].iter().cloned().collect();
        }
    }
    for line in cross {
        for s in line {
            print!("{}\t", s);
        }
        println!();
    }
}

fn main() {
    //let cfn = "crossword_CB.txt";
    //let dfn = "diccionari_CB.utf8";
    let cfn = "crossword_A.txt";
    let dfn = "diccionari_A.utf8";

    let mut cross: Vec<Vec<String>> = Vec::new();

    let f = File::open(cfn).unwrap_or_else(|_| {
        eprintln!("crossword file {} not found", cfn);
        exit(1);
    });
    let file = BufReader::new(&f);
    for line in file.lines() {
        let l = line.unwrap();
        println!("{}", l);
        cross.push(l.split_whitespace().map(|x| {x.to_string()}).collect());
    }

    let mut hwords: Vec<Word> = Vec::new();
    let mut vwords: Vec<Word> = Vec::new();
    for (linenum, line) in cross.iter().enumerate() {
        for (cellnum, cell) in line.iter().enumerate() {
            if cell == "#" || cell == "0" {
                continue;
            }

            // find horiz. word at point
            let mut i: usize = cellnum;
            let mut nodes: Vec<(usize, usize)> = Vec::new();
            nodes.push((cellnum, linenum));
            let maxlen = line.len();
            while i+1 < maxlen {
                if line[i+1] == "#" {
                    break;
                }
                i += 1;
                nodes.push((i, linenum));
            }
            if nodes.len() > 1 {
                let word = Word {
                    id: cell.to_string(),
                    direction: Direction::Horizontal,
                    nodes: nodes};
                hwords.push(word);
            }

            // find vert. word at point
            let mut i: usize = linenum;
            let mut nodes: Vec<(usize, usize)> = Vec::new();
            nodes.push((cellnum, linenum));
            let maxlen = cross.len();
            while i+1 < maxlen {
                if cross[i+1][cellnum] == "#" {
                    break;
                }
                i += 1;
                nodes.push((cellnum, i));
            }
            if nodes.len() > 1 {
                let word = Word {
                    id: cell.to_string(),
                    direction: Direction::Vertical,
                    nodes: nodes};
                vwords.push(word);
            }
        }
    }
    let hwords = del_redundants(hwords);
    let vwords = del_redundants(vwords);

    let mut dict = HashMap::new();
    for i in 0..20 {
        dict.insert(i, Vec::new());
    }
    let f = File::open(dfn).unwrap_or_else(|_| {
        eprintln!("dictionary file {} not found", dfn);
        exit(1);
    });
    let file = BufReader::new(&f);
    for line in file.lines() {
        let l: Vec<char> = line.unwrap().chars().collect();
        let len = l.len();
        dict.get_mut(&len).unwrap().push(l);
    }

    // unassigned words, and domain for each
    let mut un: HashMap<Word, HashSet<Vec<char>>> = HashMap::new();
    // restrictions for word
    let mut rs_w: HashMap<Word, Vec<Constraint>> = HashMap::new();
    // all restrictions, no duplicates
    let mut rs: Vec<Constraint> = Vec::new();

    for word in hwords.iter().chain(vwords.iter()).cloned() {
        let d = dict.get(&word.nodes.len()).unwrap().clone();
        let d: HashSet<Vec<char>> = d.iter().cloned().collect();
        un.insert(word.clone(), d);
        rs_w.insert(word, Vec::new());
    }

    for hword in &hwords {
        for vword in &vwords {
            let mut fhi = 0;
            let mut fvi = 0;
            let mut found = false;
            'outer : for (hi, pos) in hword.nodes.iter().enumerate() {
                for (vi, pos2) in vword.nodes.iter().enumerate() {
                    if pos == pos2 {
                        found = true;
                        fhi = hi;
                        fvi = vi;
                        break 'outer;
                    }
                }
            }
            if !found {
                continue;
            }
            let r = Constraint {
                hw: hword.clone(),
                vw: vword.clone(),
                hi: fhi,
                vi: fvi,
            };
            rs.push(r.clone());
            rs_w.get_mut(&hword).unwrap().push(r.clone());
            rs_w.get_mut(&vword).unwrap().push(r);
        }
    }
    let assig: HashMap<Word, Vec<char>> = HashMap::new();
    println!("solving...");
    match solve(&assig, &un, &rs_w, &rs) {
        Some(a) => {
            println!("solution:");
            print_solution(cross, a);
        },
        None => {
            println!("solution not found");
        }
    };
}
